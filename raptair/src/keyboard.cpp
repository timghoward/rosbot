// keyboard input that operates as an xbox

#include <ros/ros.h>
#include <sensor_msgs/Joy.h>

#include <stdio.h>
#include <unistd.h>
#include <termios.h>
#include <algorithm>

using namespace std;

// For non-blocking keyboard inputs
// implementation from teleop_twist_keyboard
int getch(void)
{
  int ch;
  struct termios oldt;
  struct termios newt;

  // Store old settings, and copy to new settings
  tcgetattr(STDIN_FILENO, &oldt);
  newt = oldt;

  // Make required changes and apply the settings
  newt.c_lflag &= ~(ICANON | ECHO);
  newt.c_iflag |= IGNBRK;
  newt.c_iflag &= ~(INLCR | ICRNL | IXON | IXOFF);
  newt.c_lflag &= ~(ICANON | ECHO | ECHOK | ECHOE | ECHONL | ISIG | IEXTEN);
  newt.c_cc[VMIN] = 1;
  newt.c_cc[VTIME] = 0;
  tcsetattr(fileno(stdin), TCSANOW, &newt);

  // Get the current character
  ch = getchar();

  // Reapply old settings
  tcsetattr(STDIN_FILENO, TCSANOW, &oldt);

  return ch;
}

int main(int argc, char** argv)
{
  // Init ROS node
  ros::init(argc, argv, "keyboard");
  ros::NodeHandle nh;

  // Init cmd_vel publisher
  ros::Publisher pub = nh.advertise<sensor_msgs::Joy>("joy", 1);

  double linear = 0.0;
  double angular = 0.0;

    int JOY_ANGULAR_INDEX = 3;
    int JOY_LINEAR_INDEX = 4;
    int FIRST_GEAR_BUTTON = 2;  // index within buttons int[] that indicates if the Y button (first gear) was pressed
    int SECOND_GEAR_BUTTON = 3; // X button (second gear)
    int THIRD_GEAR_BUTTON = 1;  // B button (third gear)
    int FOURTH_GEAR_BUTTON = 0; // A bugtton (fourth gear)


  while(true){

    // Create Twist message
    sensor_msgs::Joy joy;
    joy.axes = {0.0, 0.0, 0.0, 0.0, 0.0, 0.0};
    joy.buttons = {0, 0, 0, 0, 0, 0, 0, 0};

    // Get the pressed key
    char key = getch();

    // If ctrl-C (^C) was pressed, terminate the program
    if (key == '\x03') break;

    // w and x increase/decrease forward velocity, negetive moves backward
    if(key == 'w' || key == 'W') linear = min(linear+0.2, 1.0);
    if(key == 'x' || key == 'X') linear = max(linear-0.2, -1.0);
    
    // a turns left, d turns right
    if(key == 'a' || key == 'A') angular = min(angular+0.2, 1.0);
    if(key == 'd' || key == 'D') angular = max(angular-0.2, -1.0);

    // s stops all motion - both linear and angular
    if(key == 's' || key == 'S') {linear = 0.0; angular = 0.0;};

    // set these linear and angular values in message
    joy.axes[JOY_ANGULAR_INDEX] = angular;
    joy.axes[JOY_LINEAR_INDEX] = linear;

    // set gear buttons i is first, j is second, l is third, m is fourth
    if(key == 'i' || key == 'I') joy.buttons[FIRST_GEAR_BUTTON] = 1;
    if(key == 'j' || key == 'J') joy.buttons[SECOND_GEAR_BUTTON] = 1;
    if(key == 'l' || key == 'L') joy.buttons[THIRD_GEAR_BUTTON] = 1;
    if(key == 'm' || key == 'M') joy.buttons[FOURTH_GEAR_BUTTON] = 1;

    // publish that bad boy!
    pub.publish(joy);
    ros::spinOnce();
  }

  return 0;
}
