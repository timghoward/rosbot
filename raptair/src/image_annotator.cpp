

#include "raptair/image_annotator.h"


static const std::string OPENCV_WINDOW = "Image window";

class ImageAnnotator
{
  ros::NodeHandle nh_;

  // pub sub guys
  image_transport::ImageTransport it_;
  image_transport::Subscriber image_sub_;
  image_transport::Publisher image_pub_;
  ros::Subscriber detectnet_subscriber;
  ros::Subscriber robot_state_subscriber;
    
    // target detection info
    bool have_target;
    double score;
    long class_id;
    string class_name;
    double bbox_x;
    double bbox_y;
    int holiday_count;
    double bbox_width;
    double bbox_height;

    // robot state
    string state_name;
    double pose_x;
    double pose_y;
    double pose_theta;
    double velocity_x;
    double velocity_theta;



public:
  ImageAnnotator()
    : it_(nh_)
  {
    // define publishers and subscribers
    image_sub_ = it_.subscribe("/republished/image_raw", 1, &ImageAnnotator::raw_image_handler, this);
    image_pub_ = it_.advertise("/raptair/annotated_image", 1);
    detectnet_subscriber = nh_.subscribe("/detectnet/detections", 1, &ImageAnnotator::detection_handler, this);
    robot_state_subscriber = nh_.subscribe("/robot_state", 1, &ImageAnnotator::robot_state_handler, this);

    // initialize target detection variables
    have_target = false;
    score = 0.0;
    class_id = 0;
    class_name = "";
    bbox_x = 0.0;
    bbox_y = 0.0;
    holiday_count = 0;
    bbox_width = 0.0;
    bbox_height = 0.0;

    // initialixe robot state variables
    state_name = "";
    pose_x = 0.0;
    pose_y = 0.0;
    pose_theta = 0.0;
    velocity_x = 0.0;
    velocity_theta = 0.0;
  }

  ~ImageAnnotator()
  {
    
  }

  void robot_state_handler(const raptair::State::ConstPtr & msg)
  {
    state_name = msg->state_name;
    pose_x = msg->pose_x;
    pose_y = msg->pose_y;
    pose_theta = msg->pose_theta;
    velocity_x = msg->velocity_x;
    velocity_theta = msg->velocity_theta;
  }

    // handle message from target detection node
  void detection_handler(const vision_msgs::Detection2DArray::ConstPtr& msg)
  {
    // for each object detected in the message
    for (int i=0;i<msg->detections.size();i++)
    {
        vision_msgs::Detection2D detection = msg->detections[i];
        // look at each possibility for what the object could be
        for (int j=0;j<detection.results.size(); j++)
        {
            vision_msgs::ObjectHypothesisWithPose result = detection.results[j];
            // if score is high enough, determine this to be a detection
            if(result.score > MIN_SCORE ) //&& result.score > current_state.score)
            {
                vision_msgs::BoundingBox2D bbox = detection.bbox;
                have_target = true;
                class_id = result.id;
                class_name = "Goose";
                score = result.score;
                bbox_width = bbox.size_x;
                bbox_height = bbox.size_y;
                bbox_x = bbox.center.x;
                bbox_y = bbox.center.y;
                holiday_count = 0;
            }
        }
    }
  }

    // main operation of this node, when image comes in, we draw bounding box 
    // around target and label it, and put robot state info at bottom of image
    // Then annotated image is republished
  void raw_image_handler(const sensor_msgs::ImageConstPtr& msg)
  {
    cv_bridge::CvImagePtr cv_ptr;
    try
    {
      cv_ptr = cv_bridge::toCvCopy(msg, sensor_msgs::image_encodings::BGR8);
    }
    catch (cv_bridge::Exception& e)
    {
      ROS_ERROR("cv_bridge exception: %s", e.what());
      return;
    }

    // draw bounding box labeled with class and confidence score
    if(have_target)
    {
        // box
        cv::Point box_upper_left(bbox_x - (bbox_width/2.0), bbox_y - (bbox_height/2.0));
        cv::Point box_lower_right(bbox_x + (bbox_width/2.0), bbox_y + (bbox_height/2.0));
        cv::rectangle(cv_ptr->image, box_upper_left, box_lower_right, CV_RGB(150,25,200), 3);

        // box label
        cv::Point lbl_upper_left(bbox_x - (bbox_width/2.0) - 2, bbox_y - (bbox_height/2.0) - 20);
        cv::Point lbl_lower_right(bbox_x + (bbox_width/2.0) + 2, bbox_y - (bbox_height/2.0));
        cv::rectangle(cv_ptr->image, lbl_upper_left, lbl_lower_right, CV_RGB(150,25,200), -1);

        // label text
        cv::Point txt_location(bbox_x - (bbox_width/2.0) + 5, bbox_y - (bbox_height/2.0) - 2);
        stringstream label;
        label.precision(4);
        label << class_name << " " << setw(8) << score;
        cv::putText(cv_ptr->image, label.str(), txt_location, cv::FONT_HERSHEY_DUPLEX, 0.5, cv::Scalar(200, 200, 200));
    }
    
    // print state info at bottom of image
    cv::Point status_location(10, 460);
    stringstream status;
    status.precision(4);
    status << setw(12) << state_name << " ";
    status << setw(6) << pose_x << " ";
    status << setw(6) << pose_y << " ";
    status << setw(6) << pose_theta << " ";
    status << setw(6) << velocity_x << " ";
    status << setw(6) << velocity_theta << " ";
    //status << endl;
    cv::putText(cv_ptr->image, status.str(), status_location, cv::FONT_HERSHEY_DUPLEX, 0.5, cv::Scalar(200, 200, 200));

    // republish image
    image_pub_.publish(cv_ptr->toImageMsg());
  }

    // if holiday count over limit, we no longer have a target
    // otherwise, increment holiday count
    void check_detection()
    {
        if(have_target)
        {
            if(holiday_count > 5)
            {
                have_target = false;
                holiday_count = 0;
                bbox_x = 0.0;
                bbox_y = 0.0;
                bbox_width = 0.0;
                bbox_height = 0.0;
                class_id = 0;
                class_name = "";
            }
            else
            {
                holiday_count += 1;
            }
        }
    }


  // continually do detection check above at regular interval
  void start()
  {
    ros::Rate loop_rate(20);
    while(ros::ok())
    {
        check_detection();
        ros::spinOnce();
        loop_rate.sleep();
    }
  }
};


int main(int argc, char** argv)
{
  ros::init(argc, argv, "image_annotator");
  ImageAnnotator ic;
  ic.start();
  ros::spin();
  return 0;
}