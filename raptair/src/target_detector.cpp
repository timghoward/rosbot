/************************************************
 * This is a fusion of scan data and video object detection.
 * The target object in the video frame is matched to the corresponding lidar points.
 * Those points are zeroed out in the scan and the scan is republished.
 * Also, on a regular interval, the target information is published. 
 * This includes if we have a target or not, and if so, what is its distance and bearing.
 * **********************************************/

#include "raptair/target_detector.h"





// for each scan, filter out any points corresponding to target and publish filtered scan
// determine the relative distance and bearing of target and publish that too.
void scan_handler(const sensor_msgs::LaserScan & msg)
{
    // zero out any points correspnding to the target and republish
  
    sensor_msgs::LaserScan scan_msg = msg;
    raptair::Target target_msg;
    float distance_to_target = 100.0;

    if(have_target)
    {
        // expand width of bbox a bit in case it clips object
        double x_min = bbox_x - bbox_width/2.0; // - 16.0;  
        double x_max = bbox_x + bbox_width/2.0; // + 16.0;

        float PIXELS_PER_LIDAR_POINT = 10.0;

        // list of distances of all lidar points within the bounding box
        vector<float> bbox_distances;
        bbox_distances.reserve(80);

        // go through all lidar points within image frame
        for(int i=0;i<80;i++)
        {
            // is the lidar point within the bounding box
            int lidar_index = lidar_indexes_in_image[i];   // this is the lidar index, 39-0, 359-320
            double pixel = 0;
            if(lidar_index < 40)  // lidar point on left side of image
            {
                pixel = 320.0 - lidar_index * PIXELS_PER_LIDAR_POINT;
                
            } else {
                pixel = 320.0 + (360-lidar_index) * PIXELS_PER_LIDAR_POINT;
            }
            // if pixel is within bounding box, add lidar distance to list
            // and remove lidar point from scan
            if(pixel >= x_min && pixel <= x_max)
            {
                float distance = scan_msg.ranges[lidar_index];
                bbox_distances.push_back(distance);
                scan_msg.ranges[lidar_index] = numeric_limits<float>::infinity();
                scan_msg.intensities[lidar_index] = 0.0;
            }
        }
        // sort distances and get median distance
        sort(bbox_distances.begin(), bbox_distances.end());
        int median_index = bbox_distances.size() / 2;
        distance_to_target = bbox_distances[median_index];

        // populate target message
        target_msg.have_target = true;
        target_msg.distance = distance_to_target;
        target_msg.bearing = scan_msg.angle_increment * (320.0 - bbox_x) / 8.0;
    }
    else
    {
        // there is no target
        target_msg.have_target = false;
        target_msg.bearing = 0.0;
        target_msg.distance = 0.0;
    }

    // check for blocking obstacle  
    target_msg.is_blocked = false;
    if(distance_to_target > BLOCK_DISTANCE)
    {
        // look on left hand side of robot
        for(int i=0;i<20;i++)
        {
            if(scan_msg.intensities[i] > 1.0 && scan_msg.ranges[i] > scan_msg.range_min && scan_msg.ranges[i] < BLOCK_DISTANCE)
            {
                target_msg.is_blocked = true;
            }
        }
        // look on right side of robot
        for(int i=340;i<360; i++)
        {
            if(scan_msg.intensities[i] > 1.0 && scan_msg.ranges[i] > scan_msg.range_min && scan_msg.ranges[i] < BLOCK_DISTANCE)
            {
                target_msg.is_blocked = true;
            }
        }
    }

    // publish both messages
    scan_publisher->publish(scan_msg);
    target_publisher->publish(target_msg);
}


// if holiday count over limit, we no longer have a target
// otherwise, increment holiday count
void check_detection()
{
    if(have_target)
    {
        if(holiday_count > 5)
        {
            have_target = false;
            holiday_count = 0;
            bbox_x = 0.0;
            bbox_y = 0.0;
            bbox_width = 0.0;
            bbox_height = 0.0;
        }
        else
        {
            holiday_count += 1;
        }
    }
}





int main(int argc, char**  argv)
{
    // create node
    ros::init(argc, argv, "controller");
    ros::NodeHandle n;

    ros::Publisher scan_pub = n.advertise<sensor_msgs::LaserScan>("filtered_scan", 1000);
    scan_publisher = &scan_pub;

    ros::Publisher target_pub = n.advertise<raptair::Target>("target", 1000);
    target_publisher = &target_pub;

    // ros::Publisher img_pub = n.advertise<sensor_msgs::CompressedImage>("/raptair/annotated_image/compressed", 1000); 
    // annotated_image_publisher = &img_pub;

    // make a list of all lidar point indexes corresponding with the image frame from left to right
    // this will be {39, 38, ...1, 0, 359, 358...320}
    lidar_indexes_in_image.reserve(80);
    for(int i = 39;i>=0;i--) lidar_indexes_in_image.push_back(i);
    for(int i = 359;i>=320; i--) lidar_indexes_in_image.push_back(i);

    ros::Subscriber detectnet_subscriber = n.subscribe("/detectnet/detections", 10, detection_handler); 
    ros::Subscriber scan_subscriber = n.subscribe("/scan", 10, scan_handler);
    // ros::Subscriber image_subscriber = n.subscribe("/republished/image_raw", 10, raw_image_handler);

    // start main loop which checks status of detected target
    ros::Rate loop_rate(20);
    while(ros::ok())
    {
        check_detection();
        ros::spinOnce();
        loop_rate.sleep();
    }

    ros::spin();
    return 0;
}



// handle detectnet object detection in video frame
void detection_handler(const vision_msgs::Detection2DArray::ConstPtr& msg)
{
    // for each object detected in the message
    for (int i=0;i<msg->detections.size();i++)
    {
        vision_msgs::Detection2D detection = msg->detections[i];
        // look at each possibility for what the object could be
        for (int j=0;j<detection.results.size(); j++)
        {
            vision_msgs::ObjectHypothesisWithPose result = detection.results[j];
            // if score is high enough, determine this to be a detection
            if(result.score > MIN_SCORE ) //&& result.score > current_state.score)
            {
                vision_msgs::BoundingBox2D bbox = detection.bbox;
                have_target = true;
                class_id = result.id;
                score = result.score;
                bbox_width = bbox.size_x;
                bbox_height = bbox.size_y;
                bbox_x = bbox.center.x;
                bbox_y = bbox.center.y;
                holiday_count = 0;
            }
        }
    }
}