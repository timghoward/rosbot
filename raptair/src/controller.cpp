#include "raptair/controller.h"

#include <stdio.h>
#include <unistd.h>
#include <termios.h>

ros::Publisher* cmd_vel_publisher = NULL;
ros::Publisher* robot_state_publisher = NULL;
MoveBaseClient* move_base_client = NULL;
tf2_ros::Buffer tfBuffer;

// Called once when the goal completes
void doneCb(const actionlib::SimpleClientGoalState& state,
            const move_base_msgs::MoveBaseResultConstPtr& result)
{
  ROS_INFO("Finished in state [%s]", state.toString().c_str());
}

// Called once when the goal becomes active
void activeCb()
{
  ROS_INFO("Goal just went active");
}

// Called every time feedback is received for the goal
void feedbackCb(const move_base_msgs::MoveBaseFeedbackConstPtr& feedback)
{
    double x = feedback->base_position.pose.position.x;
    double y = feedback->base_position.pose.position.y;
    double z = feedback->base_position.pose.orientation.z;
    double w = feedback->base_position.pose.orientation.w;
    double theta = 0.0;

    ROS_INFO("Current Pose:  x: %f  y: %f  theta: %f", x, y, theta);
    // http://docs.ros.org/fuerte/api/move_base_msgs/html/msg/MoveBaseActionFeedback.html
}

void saveMap()
{
    string command = "rosrun map_server map_saver -f " + map_file;
    system(command.c_str());
}

// Transition from current state to next state based on the current state and
// any new information that has come in (pose info, target info, requests for state change).
RobotState transition_to_next_state()
{
    print_state();

    // Copy only state name, location, and velocity.  All else set in logic below.
    RobotState next_state = RobotState(current_state);

    // see if we need to move into blocked state
    if(current_state.scan_shows_block && current_state.state != STATE_BLOCKED)
    {
        next_state.state_before_blocked = current_state.state;
        next_state.state = STATE_BLOCKED;
        next_state.velocity_x = 0.0;
        next_state.velocity_theta = 0.0;
        return next_state;
    }

    // see if we need to move out of blocked state and into previous state
    if(current_state.state == STATE_BLOCKED )
    {
        if( ! current_state.scan_shows_block)
        {
            next_state.state = next_state.state_before_blocked;
            next_state.state_before_blocked = STATE_NONE;
        }
        return next_state;
    }

    // handle any request for change of state
    // New state will not be handled until next pass through this loop.
    if(current_state.requested_state != STATE_NONE)
    {
        // only these states are allowed for request, all other requested states will be ignored
        if(current_state.requested_state == STATE_IDLE || current_state.requested_state == STATE_SEARCHING
        || current_state.requested_state == STATE_MANUAL || current_state.requested_state == STATE_MAPPING
        || current_state.requested_state == STATE_NAVIGATING)
        {
            next_state.state = current_state.requested_state;
            next_state.requested_state = STATE_NONE;
            // if going into navigating state, then capture additional parameters
            if(current_state.requested_state == STATE_NAVIGATING)
            {

            }
            return next_state;
        }
        // if saving map is requested, then save map and continue process state transition as usual
        if(current_state.requested_state == STATE_SAVING_MAP)
        {
            next_state.requested_state = STATE_NONE;
            saveMap();
        }
    }

    // If we have a detection and we are not already in pursuing state (and not in manual state either)
    // then transition to PURSUING state.
    if(current_state.has_target && current_state.state != STATE_PURSUING && current_state.state != STATE_MANUAL)
    {
        next_state.state = STATE_PURSUING;
        move_robot_towards_target(next_state);
        return next_state;
    }

    // If idle, just make sure velocities are zero.
    if(current_state.state == STATE_IDLE)
    {
        next_state.velocity_x = 0.0; next_state.velocity_theta = 0.0;
        return next_state;
    }

    // searching is a 360 spin in hopes of acquiring a target.  
    // If robot acquires target, then transition into pursuing state.
    // If robot finishes 360 spin with no target, transition to idle state.
    if(current_state.state == STATE_SEARCHING)
    {
        if(previous_state.state == STATE_SEARCHING)
        {
            // if already searching then continue searching (that is, continue spinnig).
            next_state.velocity_theta = 0.5; 
            next_state.velocity_x = 0.0;
            next_state.start_search_time = current_state.start_search_time;
            next_state.final_bearing = current_state.final_bearing;

            // Test for search completion and end search if done.
            // Since the target bearing is the same as the initial bearing (360 spin)
            // then wait five seconds before initially conducting this test so that the 
            // robot has time to rotate away from original bearing.  Otherwise, search 
            // will stop as soon as it starts!
            double current_seconds = ros::Time::now().toSec();
            if(current_seconds > (current_state.start_search_time + 5.0))
            {
                if(abs(current_state.pose_theta-current_state.final_bearing) < 0.05)
                {
                    next_state.velocity_x = 0.0; next_state.velocity_theta = 0.0;
                    next_state.final_bearing = 0.0; 
                    next_state.start_search_time = 0.0;
                    next_state.state = STATE_IDLE;
                }
            }
        }
        else
        {
            // if previous state was not searching, then set up values to start searching
            next_state.velocity_theta = 0.5; next_state.velocity_x = 0.0;
            next_state.final_bearing == current_state.pose_theta;
            next_state.start_search_time = ros::Time::now().toSec();
        }
        return next_state;
    }

    // Handle pursuing state.  If robot still has target, keep after target.
    // Once we lose the target, go to searching state.
    if(current_state.state == STATE_PURSUING)
    {
        if(current_state.has_target)
        {
            move_robot_towards_target(next_state);
        }
        else  
        {
            next_state.state = STATE_SEARCHING;
            next_state.velocity_x = 0.0; next_state.velocity_theta = 0.0;
        }

        return next_state;
    }

    // Handle navigating state.  
    if(current_state.state == STATE_NAVIGATING)
    {
        // first time through with NAVIGATING state, send goal to move_base
        if(previous_state.state != STATE_NAVIGATING)
        {
            tf2::Quaternion quat;
            quat.setRPY(0, 0, current_state.goal_theta);
            move_base_msgs::MoveBaseGoal goal;

            //we'll send a goal to the robot to move 1 meter forward
            goal.target_pose.header.frame_id = "base_link";
            goal.target_pose.header.stamp = ros::Time::now();

            goal.target_pose.pose.position.x = 1.0;
            goal.target_pose.pose.orientation.z = quat.getZ();
            goal.target_pose.pose.orientation.w = quat.getW();

            //move_base_client->sendGoal(goal, &doneCb, &activeCb, &feedbackCb);
            move_base_client->sendGoal(goal, 
                                        &doneCb, 
                                        &activeCb,
                                        &feedbackCb); 
        }
    }

    // handle manual and mapping states
    if(current_state.state == STATE_MANUAL || current_state.state == STATE_MAPPING)
    {
        if(current_state.has_joy_msg)
        {
            // for now, use these constants in calculations
            double JOY_ANGULAR_SCALE = 1.0;
            double JOY_LINEAR_SCALE = 0.25;
            int JOY_ANGULAR_INDEX = 3;
            int JOY_LINEAR_INDEX = 4;
            int FIRST_GEAR_BUTTON = 2;  // index within buttons int[] that indicates if the Y button (first gear) was pressed
            int SECOND_GEAR_BUTTON = 3; // X button (second gear)
            int THIRD_GEAR_BUTTON = 1;  // B button (third gear)
            int FOURTH_GEAR_BUTTON = 0; // A bugtton (fourth gear)

            // capture current joy gear
            int gear = current_state.joy_gear;

            // see if we are changing gears
            if(current_state.joy_buttons[FIRST_GEAR_BUTTON] == 1) {
                gear = 1;
            } else if(current_state.joy_buttons[3] == 1) {
                gear = 2;
            } else if(current_state.joy_buttons[1] == 1) {
                gear = 3;
            } else if(current_state.joy_buttons[0] == 1) {
                gear = 4;
            }

            // set velocities to control robot
            next_state.velocity_x = gear * JOY_LINEAR_SCALE * current_state.joy_axes[JOY_LINEAR_INDEX];
            cout << "new velocity_x=" << next_state.velocity_x << endl;
            next_state.velocity_theta =  JOY_ANGULAR_SCALE * current_state.joy_axes[JOY_ANGULAR_INDEX];
            next_state.joy_gear = gear;
        }
        return next_state;
    }


    return next_state;
}


// Set both velocities to move robot towards target.
// Since camera is fix mounted, if we have a target, then it is more or
// less directly in front of robot so set positive forward velocity.
// Adjust theta so that bearing of target relative to robot is not 
// too far off center.
void move_robot_towards_target(RobotState & next_state)
{
    if( current_state.target_bearing > BEARING_TOLERANCE)
    {
        next_state.velocity_x = PURSUIT_VELOCITY;
        next_state.velocity_theta = PURSUIT_THETA;
    }
    else if( current_state.target_bearing < -BEARING_TOLERANCE)
    {
        next_state.velocity_x = PURSUIT_VELOCITY;
        next_state.velocity_theta = -PURSUIT_THETA;
    }
    else
    {
        next_state.velocity_x = PURSUIT_VELOCITY;
        next_state.velocity_theta = 0.0;
    } 
}

void set_pursuit_values()
{
    if(pursuit_gear == 1)
    {
        PURSUIT_THETA = 0.1;
        PURSUIT_VELOCITY = 0.05;
    }
    else if(pursuit_gear == 3)
    {
        PURSUIT_THETA = 0.3;
        PURSUIT_VELOCITY = 0.5;
    }
    else if(pursuit_gear == 4)
    {
        PURSUIT_THETA = 0.5;
        PURSUIT_VELOCITY = 0.8;
    }
    else // second gear is the default
    {
        PURSUIT_THETA = 0.2;
        PURSUIT_VELOCITY = 0.2;       
    }
}


// if velocities changed from previous state, then send new velocities to controller
void control_robot()
{
    if(current_state.velocity_theta != previous_state.velocity_theta || current_state.velocity_x != previous_state.velocity_x)
    {
        geometry_msgs::Twist twist;
        twist.angular.z = current_state.velocity_theta;
        twist.linear.x = current_state.velocity_x;
        cmd_vel_publisher->publish(twist);
    }
}


// Get latest pose of robot and update current state with it.
void get_pose()
{
    try
    {
        geometry_msgs::TransformStamped transformStamped;
        transformStamped = tfBuffer.lookupTransform("odom", "base_link", ros::Time(0));
        current_state.pose_x = transformStamped.transform.translation.x;
        current_state.pose_y = transformStamped.transform.translation.y;
        geometry_msgs::Quaternion pose_quat = transformStamped.transform.rotation;
        current_state.pose_quat_z = pose_quat.z;
        current_state.pose_quat_w = pose_quat.w;
        tf::Quaternion quat(pose_quat.x, pose_quat.y, pose_quat.z, pose_quat.w);
        tf::Matrix3x3 m(quat);
        double roll, pitch, yaw; 
        m.getRPY(roll, pitch, yaw);
        current_state.pose_theta = yaw;
        //ROS_INFO("x: %f  y: %f  theta: %f", current_state.pose_x, current_state.pose_y, current_state.pose_theta);
    }
    catch (tf2::TransformException ex)
    {
        ROS_WARN("%s",ex.what());
        ros::Duration(1.0).sleep();
    }
}

void requested_state_handler(const raptair::State::ConstPtr& msg)
{
    cout << msg->state_name << " " << msg->state_id << endl;
    current_state.requested_state = convert_state_name_to_state(msg->state_name.c_str());
    if(current_state.requested_state == STATE_NAVIGATING)
    {
        current_state.goal_x = msg->pose_x;
        current_state.goal_y = msg->pose_y;
        current_state.goal_theta = msg->pose_theta;
        current_state.goal_status = "REQUESTED";
    }
}

void target_handler(const raptair::Target::ConstPtr& msg)
{
    current_state.has_target = msg->have_target;
    current_state.target_bearing = msg->bearing;
    current_state.target_distance = msg->distance;
    current_state.scan_shows_block = msg->is_blocked;
}

void joy_handler(const sensor_msgs::Joy::ConstPtr& joy )
{
    cout << "JOY MESSAGE" << endl;
    current_state.joy_axes = joy->axes;
    current_state.joy_buttons = joy->buttons;
    current_state.has_joy_msg = true;
}

int row = 10;
void print_state()
{
    if(row == 10)
    {
        cout << endl << "       STATE      X      Y  THETA   V_X  V_TH  TARGT   DIST   BRNG"  << endl;
        row = 0;
    }
    cout.precision(4);
    
    cout << setw(12) << convert_state_to_state_name(current_state.state) << " ";

    cout << setw(6) << current_state.pose_x << " ";
    cout << setw(6) << current_state.pose_y << " ";
    cout << setw(6) << current_state.pose_theta << " ";

    cout << setw(5) << current_state.velocity_x << " ";
    cout << setw(5) << current_state.velocity_theta << " ";

    // cout << setw(6) << current_state.final_bearing << " ";

    cout << setw(6) << (current_state.has_target ? "TRUE" : "FALSE") << " " ;
    cout << setw(6) << current_state.target_distance  << " ";
    cout << setw(6) << current_state.target_bearing << " ";

    //cout << setw(16) << (current_state.scan_shows_block ? "BLOCKED" : "");
    
    cout << endl;

    row++;
}


void publish_robot_state()
{
    raptair::State robot_state;
    robot_state.state_name = convert_state_to_state_name(current_state.state);
    robot_state.state_id = current_state.state;
    robot_state.pose_x = current_state.pose_x;
    robot_state.pose_y = current_state.pose_y;
    robot_state.pose_theta = current_state.pose_theta;
    robot_state.velocity_x = current_state.velocity_x;
    robot_state.velocity_theta = current_state.velocity_theta;

    robot_state_publisher->publish(robot_state);
}



int main(int argc, char**  argv)
{
    // create node
    ros::init(argc, argv, "controller");
    ros::NodeHandle n;
    ros::NodeHandle private_n("~");

    // get arguments and parameters
    private_n.param<std::string>("nav_mode", nav_mode, "slam");
    private_n.param<std::string>("map_file", map_file, "maps/default.yaml");
    private_n.param<std::string>("manual", manual, "none");
    private_n.param<int>("pursuit_gear", pursuit_gear, 2);

    set_pursuit_values();

    // create publishers and subscribers
    ros::Publisher pub = n.advertise<geometry_msgs::Twist>("cmd_vel", 1000);
    cmd_vel_publisher = &pub;
    ros::Publisher st_pub = n.advertise<raptair::State>("robot_state", 1000);
    robot_state_publisher = &st_pub;
    tf2_ros::TransformListener listener(tfBuffer);
    ros::Subscriber state_subscriber = n.subscribe("/requested_state", 10, requested_state_handler);
    ros::Subscriber target_subscriber = n.subscribe("/target", 10, target_handler);
    ros::Subscriber joy_subscriber = n.subscribe("/joy", 5, joy_handler);

    // make sure robot is full stop by setting previous state velocity_x to something
    // other than zero which will send current state velocities of 0 to controller
    // CURRENTLY NOT WORKING subscriber not ready yet
    previous_state.velocity_x = 0.001;
    control_robot();

    // if in navigation mode, create move_base client and wait for server
    if(nav_mode == "nav")
    {
        //tell the action client that we want to spin a thread by default
        MoveBaseClient ac("move_base", true);
        move_base_client = &ac;
        while(!ac.waitForServer(ros::Duration(5.0))){
            ROS_INFO("Waiting for the move_base action server to come up");
        }
    }

    // start main loop that updates robot state on regular basis
    // determines new state based on current state and any new information that 
    // has come in since last time through the loop
    ros::Rate loop_rate(10);
    while(ros::ok())
    {
        get_pose();
        RobotState next_state = transition_to_next_state();
        previous_state = current_state;
        current_state = next_state;
        control_robot();
        publish_robot_state();
        ros::spinOnce();
        loop_rate.sleep();
    }
    
    ros::spin();
    return 0;
}


