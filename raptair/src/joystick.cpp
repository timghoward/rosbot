#include <ros/ros.h>
#include <geometry_msgs/Twist.h>
#include <sensor_msgs/Joy.h>

using namespace std;

class Joystick
{
public:
  Joystick();

private:
  void joyCallback(const sensor_msgs::Joy::ConstPtr& joy);

  ros::NodeHandle nh_;

  int linear_, angular_;
  double l_scale_, a_scale_;
  int gear_;
  ros::Publisher vel_pub_;
  ros::Subscriber joy_sub_;
};


Joystick::Joystick():
  linear_(4),
  angular_(3),
  gear_(1)
{

  nh_.param("axis_linear", linear_, linear_);
  nh_.param("axis_angular", angular_, angular_);
  nh_.param("scale_angular", a_scale_, a_scale_);
  nh_.param("scale_linear", l_scale_, l_scale_);

  vel_pub_ = nh_.advertise<geometry_msgs::Twist>("cmd_vel", 1);
  joy_sub_ = nh_.subscribe<sensor_msgs::Joy>("joy", 10, &Joystick::joyCallback, this);
}


void Joystick::joyCallback(const sensor_msgs::Joy::ConstPtr& joy)
{
  geometry_msgs::Twist twist;

  // see if we are chaning gears
  if(joy->buttons[2] == 1)
  {
      //ROS_INFO("First Gear");
      gear_ = 1;
  } else if(joy->buttons[3] == 1) {
      //ROS_INFO("Second Gear");
      gear_ = 2;
  } else if(joy->buttons[1] == 1) {
      //ROS_INFO("Third Gear");
      gear_ = 3;
  } else if(joy->buttons[0] == 1) {
      //ROS_INFO("Fourth Gear");
      gear_ = 4;
  }

  // calculate and publish twist
  twist.angular.z = a_scale_ * joy->axes[angular_];
  twist.linear.x = gear_ * l_scale_ * joy->axes[linear_];

  vel_pub_.publish(twist);
}


int main(int argc, char** argv)
{
  ros::init(argc, argv, "joystick");
  Joystick joystick;

  ros::spin();
}