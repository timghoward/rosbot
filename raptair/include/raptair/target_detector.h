#include "ros/ros.h"
#include "raptair/Target.h"
#include <bits/stdc++.h>
#include <vision_msgs/Detection2DArray.h>
#include <sensor_msgs/LaserScan.h>
#include <limits>

#include "raptair/utils.h"


using namespace std;


ros::Publisher* scan_publisher = NULL;
ros::Publisher* target_publisher = NULL;
ros::Publisher* annotated_image_publisher = NULL;


double BLOCK_DISTANCE = 0.5;


bool have_target = false;
double score = 0.0;
long class_id = 0;
double bbox_x = 0.0;
double bbox_y = 0.0;
int holiday_count = 0;
double bbox_width = 0.0;
double bbox_height = 0.0;


// int indexes[] = {39, 38, 37, 36, 35, 34, 33, 32, 31, 30, 29, 28, 27, 26, 25, 24, 23, 22, 21, 20,
// 19, 18, 17, 16, 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0,
// 359, 358, 357, };


vector<int> lidar_indexes_in_image;


void detection_handler(const vision_msgs::Detection2DArray::ConstPtr& msg);

void scan_handler(const sensor_msgs::LaserScan & msg);

void check_detection();


