#include "ros/ros.h"
#include "std_msgs/String.h"
#include "raptair/State.h"
#include "raptair/Target.h"
#include "raptair/utils.h"
#include <tf2_ros/transform_listener.h>
#include <geometry_msgs/TransformStamped.h>
#include <geometry_msgs/Twist.h>
#include <sensor_msgs/Joy.h>
#include <vision_msgs/Detection2DArray.h>
#include <tf/transform_datatypes.h>
#include <move_base_msgs/MoveBaseAction.h>
#include <actionlib/client/simple_action_client.h>
#include <sstream>
#include "ros/package.h"


using namespace std;

typedef actionlib::SimpleActionClient<move_base_msgs::MoveBaseAction> MoveBaseClient;

// describes robot current state including named state, location, bearing, 
// linear and angular velocity, subscription data that has come in since last
// state transition, and other specialized values used for specific states
struct RobotState {
    int state;                  // current state of robot
    int state_before_blocked;   // state robot was in before getting blocked, return to this state when unblocked

    // robot pose
    double pose_x;      // robot's x location on grid map
    double pose_y;      // robot's y location on grid map
    double pose_theta;  // robot's bearing as radians
    double pose_quat_z; // robot's bearing as quaternions (x and y quat values are always 0)
    double pose_quat_w;

    // command velocities
    double velocity_x;
    double velocity_theta;

    // requested state
    int requested_state;

    // search values
    double start_search_time;  // time of start of search
    double final_bearing;     // final bearing - same as initial bearing since we are doing a 360 spin

    // pursuing values
    bool has_target;
    bool scan_shows_block;
    double target_distance;
    double target_bearing;

    // navigation values
    double goal_x;
    double goal_y;
    double goal_theta;
    string goal_status;

    // manual joy values
    int joy_gear;   // 1, 2, 3, or 4
    bool has_joy_msg;
    std::vector<float> joy_axes;
    std::vector<int> joy_buttons;

    RobotState()
    {
        state = STATE_NONE;
        state_before_blocked = STATE_NONE;
        requested_state = STATE_NONE;
        has_joy_msg = false;
        joy_gear = 1;
    }

    RobotState(const RobotState & sourceRobotState)
    {
        state = sourceRobotState.state;
        state_before_blocked = sourceRobotState.state_before_blocked;

        pose_x = sourceRobotState.pose_x;
        pose_y = sourceRobotState.pose_y;
        pose_theta = sourceRobotState.pose_theta;
        pose_quat_z = sourceRobotState.pose_quat_z;
        pose_quat_w = sourceRobotState.pose_quat_w;

        velocity_x = sourceRobotState.velocity_x;
        velocity_theta = sourceRobotState.velocity_theta;

        requested_state = STATE_NONE;

        start_search_time = 0.0;
        final_bearing = 0.0;

        has_target = sourceRobotState.has_target;
        scan_shows_block = sourceRobotState.scan_shows_block;
        target_bearing = sourceRobotState.target_bearing;
        target_distance = sourceRobotState.target_distance;

        goal_x = sourceRobotState.goal_x;
        goal_y = sourceRobotState.goal_y;
        goal_theta = sourceRobotState.goal_theta;
        goal_status = sourceRobotState.goal_status;

        joy_gear = sourceRobotState.joy_gear;
        has_joy_msg = false;
    }
};


// robot has a current state and previous state
RobotState current_state;
RobotState previous_state;


// parameters
string nav_mode;   // none, slam, nav, or map
string map_file;   // full path for yaml file
string manual;     // none, kbd, joy - how user drives robot in manual mode
int pursuit_gear;  // 1, 2, 3, or 4 - how aggressively we pursue target


// static values (perhaps initialized at runtime)
double BEARING_TOLERANCE = 0.05;
double PURSUIT_THETA = 0.1;
double PURSUIT_VELOCITY = 0.2;


// functions
RobotState transition_to_next_state();

void move_robot_towards_target(RobotState & next_state);

void control_robot();

void print_state();

void copy_detection_info(RobotState & next_state);

int getch(void);


