
#ifndef UTILS_H
#define UTILS_H

#include <string>


// valid states
int STATE_NONE = 0;         // unknown state
int STATE_UNKNOWN = 0;      // unknown state
int STATE_IDLE = 1;         // robot is sitting there doing nothing
int STATE_MANUAL = 2;       // robot is being driven from keyboard and will not apply any other logic until put into another state
int STATE_SEARCHING = 3;    // robot is rotating 360 trying to locate a target
int STATE_PURSUING = 4;     // robot has a target in view and is going towards it (may be using nav stack)
int STATE_NAVIGATING = 5;   // robot is moving from original location to destination using nav stack, does not have a target
int STATE_RETURNING = 6;    // robot is returning to home base, kind of specialized NAVIGATION state
int STATE_BLOCKED = 7;      // robot has obstacle in front and is stopped until clear again
int STATE_MAPPING = 8;      // robot is being driven by user to build a map
int STATE_SAVING_MAP = 9;   // does not actually change state, more of a command, simply saves current map to file

double MIN_SCORE = 0.5;

double IMAGE_HEIGHT = 480.0;
double IMAGE_WIDTH = 640.0;





// converts a string name of a state to its int value
int convert_state_name_to_state(const char* state_name)
{
    if(strcmp(state_name, "IDLE") == 0) return STATE_IDLE;
    if(strcmp(state_name, "MANUAL") == 0) return STATE_MANUAL;
    if(strcmp(state_name, "SEARCHING") == 0) return STATE_SEARCHING;
    if(strcmp(state_name, "PURSUING") == 0) return STATE_PURSUING;
    if(strcmp(state_name, "NAVIGATING") == 0) return STATE_NAVIGATING;
    if(strcmp(state_name, "RETURNING") == 0) return STATE_RETURNING;
    if(strcmp(state_name, "BLOCKED") == 0) return STATE_BLOCKED;
    if(strcmp(state_name, "MAPPING") == 0) return STATE_MAPPING;
    if(strcmp(state_name, "SAVING_MAP") == 0) return STATE_SAVING_MAP;

    return STATE_NONE; 
}

const char* convert_state_to_state_name(int state)
{
    if(state == STATE_NONE) return "NONE";
    if(state == STATE_UNKNOWN) return "UNKNOWN";
    if(state == STATE_IDLE) return "IDLE";
    if(state == STATE_MANUAL) return "MANUAL";
    if(state == STATE_SEARCHING) return "SEARCHING";
    if(state == STATE_PURSUING) return "PURSUING";
    if(state == STATE_NAVIGATING) return "NAVIGATING";
    if(state == STATE_RETURNING) return "RETURNING";
    if(state == STATE_BLOCKED) return "BLOCKED";
    if(state == STATE_MAPPING) return "MAPPING";
    if(state == STATE_SAVING_MAP) return "SAVING_MAP";
    return "UNKNOWN";
}


#endif