#include <ros/ros.h>
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>
#include <opencv2/imgproc/imgproc.hpp>
#include <vision_msgs/Detection2DArray.h>
#include <string>

#include "raptair/State.h"

#include "raptair/utils.h"

using namespace std;

